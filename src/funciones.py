# encoding=utf-8
# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

def funcion1(n):
    for i in range(n):
        print("Programando que es gerundio")

def funcion2(n1, n2):
    if n1>=n2:
        return n1
    else:
        return n2
    
def funcion3(n1, n2, n3):
    if n1>=n2 and n1>n3: #n1 es el mayor
        return n1
    elif n2>n3:
        return n2
    else:
        return n3

def funcion4(miLista):
    num=0
    max=miLista[0]
    for num in miLista:
        if num>max: max=num
    return max

if __name__ == "__main__":
    mi_lista=[112,32,7,15,77,46,54]
    print(funcion4(mi_lista))
